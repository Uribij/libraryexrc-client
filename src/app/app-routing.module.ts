import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInScreenComponent } from './log-in-screen/log-in-screen.component';
import { ManagementAndCreationComponent } from './management-and-creation/management-and-creation.component';
import { NeedAuthGuard } from './needAuthGuard';
import { UserManagementComponent } from './user-management/user-management.component';

const routes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  { path: 'login', component: LogInScreenComponent },
  { path: 'users', component: UserManagementComponent,canActivate: [NeedAuthGuard] },
  { path: 'books', component: ManagementAndCreationComponent,canActivate: [NeedAuthGuard],data:{managingType:"books"} },
  { path: 'authors', component: ManagementAndCreationComponent,canActivate: [NeedAuthGuard],data:{managingType:"authors"} },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
