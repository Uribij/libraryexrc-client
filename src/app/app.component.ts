import { Component } from '@angular/core';
import { AuthorService } from './author.service';
import { BookService } from './book.service';
import { User } from './userDto';
import { UsersService } from './users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'libraryClient';
  constructor(
    private readonly usersService: UsersService,
    private readonly authorService: AuthorService,
    private readonly bookService: BookService
  ) {
    bookService.getBooks();
    authorService.getAuthors();
    this.usersService.updateAllUsers();
    this.usersService.followLoginStatus().subscribe((status) => {
      this.isLogged = !!status;
      this.currentUser = status ? status : this.currentUser;
    });
  }
  currentUser: User;
  isLogged!: boolean;
  tabs = [
    { name: 'ניהול משתמשים', route: '/users' },
    { name: 'ניהול ספרים', route: '/books' },
    { name: 'ניהול סופרים', route: '/authors' },
  ];

  private logOut() {
    this.usersService.logOut();
  }
}
