import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogInScreenComponent } from './log-in-screen/log-in-screen.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserManagementComponent } from './user-management/user-management.component';
import { NeedAuthGuard } from './needAuthGuard';
import { BookDataPipe } from './book-data.pipe';
import { MaterialModuleModule } from './material-module/material-module.module';
import { AsyncPipe, CommonModule } from '@angular/common';
import { AuthorDataPipe } from './author-data.pipe';
import { BookListDialougeComponent } from './book-list-dialouge/book-list-dialouge.component';
import { ManagementAndCreationComponent } from './management-and-creation/management-and-creation.component';
import { FormDialogueComponent } from './form-dialogue/form-dialogue.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LogInScreenComponent,
    UserManagementComponent,
    BookDataPipe,
    AuthorDataPipe,
    BookListDialougeComponent,
    ManagementAndCreationComponent,
    FormDialogueComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModuleModule,
    CommonModule,
  ],
  providers: [NeedAuthGuard, AsyncPipe, BookDataPipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
