import { Pipe, PipeTransform } from '@angular/core';
import { AuthorService } from './author.service';

@Pipe({
  name: 'authorNameById',
})
export class AuthorDataPipe implements PipeTransform {
  constructor(private authorService: AuthorService) {}

  async transform(authorId: number): Promise<string> {

    const author = await this.authorService.getAuthorById(+authorId);
    return !(+authorId)?"אין סופר":author ? author.authorName : 'הסופר לא במאגר הנתונים';
  }
}
