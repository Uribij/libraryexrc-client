import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Author } from './author.dto';

@Injectable({
  providedIn: 'root',
})
export class AuthorService {
  private authorsUrl = 'http://localhost:3000/author';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient) {}
  private Authors!: Author[];
  private AuthorsBS = new BehaviorSubject<Author[]>(undefined);
  private requested = false;

  getAuthors(): Promise<Author[]> {
    if (!this.requested) {
      this.getAuthorsFromServer();
    }
    return new Promise((resolve) => {
      const checkInterval = setInterval(() => {
        if (this.Authors) {
          clearInterval(checkInterval);
          resolve(this.Authors);
        }
      }, 50);
    });
  }

  private getAuthorsFromServer(): void {
    this.http.get<Author[]>(this.authorsUrl).subscribe((authors) => {
      if (authors) {
        this.requested = !!authors;
        this.Authors = authors;
        this.AuthorsBS.next(authors);
      }
    });
  }

  async getAuthorById(id: number): Promise<Author> {
    return (await this.getAuthors()).filter(
      (author: Author) => author.authorId == id
    )[0];
  }

  addAuthor(author: Author): void {
    this.http
      .post<Author>(this.authorsUrl, author, this.httpOptions)
      .subscribe((answer) => (answer ? this.addAuthorToCache(answer) : {}));
  }

  updateAuthor(author: Author): void {
    this.http
      .put<Author>(this.authorsUrl, author, this.httpOptions)
      .subscribe((answer) => (answer ? this.updateAuthorInCache(answer) : {}));
  }

  deleteAuthor(author: Author): void {
    this.http
      .delete<boolean>(this.authorsUrl + '/' + author.authorId)
      .subscribe((serverAnswer: boolean) =>
        serverAnswer ? this.deleteAuthorFromCache(author.authorId) : {}
      );
  }

  getAuthorBehaviorSubject(): BehaviorSubject<Author[]> {
    return this.AuthorsBS;
  }

  updateAuthorProperty(
    authorId: number,
    replacementData: { [propertyName: string]: string | number }
  ): void {
    this.http
      .put<Author>(this.authorsUrl + '/' + authorId, replacementData)
      .subscribe((answer) => (answer ? this.updateAuthorInCache(answer) : {}));
  }

  private updateAuthorInCache(author: Author) {

    const modifiedBookList = [...this.Authors];
    modifiedBookList.splice(
      modifiedBookList
        .map((author) => author.authorId)
        .indexOf(author.authorId),
      1,
      author
    );
    this.Authors = [...modifiedBookList];
    this.AuthorsBS.next([...modifiedBookList]);
  }

  private addAuthorToCache(authorToAdd: Author) {
    this.Authors = [...this.Authors, authorToAdd];
    this.AuthorsBS.next(this.Authors);
  }

  private deleteAuthorFromCache(authorId: number) {
    const modifiedAuthorsList = [...this.Authors];
    modifiedAuthorsList.splice(
      modifiedAuthorsList
        .map((author: Author) => author.authorId)
        .indexOf(authorId),
      1
    );
    this.Authors = modifiedAuthorsList;
    this.AuthorsBS.next(modifiedAuthorsList);
  }
}
