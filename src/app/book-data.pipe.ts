import { Pipe, PipeTransform } from '@angular/core';
import { BookService } from './book.service';

@Pipe({
  name: 'getBookPropertyById'
})
export class BookDataPipe implements PipeTransform {
  constructor(private bookService: BookService) {}

  async transform(bookId: string, targetProperty: string): Promise<string> {
    const book = await this.bookService.getBookById(+bookId);
    return book?book[targetProperty]:"ספר לא קיים במאגר נתונים";
  }

}
