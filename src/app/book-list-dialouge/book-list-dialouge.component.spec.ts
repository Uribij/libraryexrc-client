import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookListDialougeComponent } from './book-list-dialouge.component';

describe('BookListDialougeComponent', () => {
  let component: BookListDialougeComponent;
  let fixture: ComponentFixture<BookListDialougeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookListDialougeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListDialougeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
