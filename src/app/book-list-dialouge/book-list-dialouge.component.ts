import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BookService } from '../book.service';
import { Book } from '../bookDto';

@Component({
  selector: 'app-book-list-dialouge',
  templateUrl: './book-list-dialouge.component.html',
  styleUrls: ['./book-list-dialouge.component.scss'],
})
export class BookListDialougeComponent {
  constructor(
    private bookService: BookService,
    private dialogRef: MatDialogRef<BookListDialougeComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      callBack: (booksToAdd: number[]) => void;
      userBookList: number[];
    }
  ) {
    this.bookService
      .getBookStatus()
      .subscribe((books) =>
        books
          ? (this.addableBooks = books.filter(
              (book) => !this.data.userBookList.includes(book.bookId)
            ))
          : {}
      );
  }

  addableBooks!: Book[];
  selectedBooks: number[] = [];

  onClick(bookId:number){
    if(this.selectedBooks.includes(bookId)){
      this.selectedBooks.splice(this.selectedBooks.indexOf(bookId),1);
    }else{
      this.selectedBooks.push(bookId);
    }
  }

  onConfirm() {
    this.data.callBack(this.selectedBooks);
    this.dialogRef.close();
  }
}
