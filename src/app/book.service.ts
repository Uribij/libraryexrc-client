import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Book } from './bookDto';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  private booksUrl = 'http://localhost:3000/book';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient,private userService:UsersService) {}
  private Books!: Book[];
  private requested = false;
  private BooksBehaviorSubject: BehaviorSubject<Book[]> = new BehaviorSubject<
    Book[]
  >([]);

  getBooks(): Promise<Book[]> {
    if (!this.requested) {
      this.getBooksFromServer();
    }
    return new Promise((resolve) => {
      const checkInterval = setInterval(() => {
        if (this.Books) {
          clearInterval(checkInterval);
          resolve(this.Books);
        }
      }, 50);
    });
  }

  private getBooksFromServer(): void {
    this.http.get<Book[]>(this.booksUrl).subscribe((books) => {
      if (books) {
        this.requested = !!books;
        this.Books = books;
        this.BooksBehaviorSubject.next(books);
      }
    });
  }

  async getBookById(id: number): Promise<Book> {
    return (await this.getBooks()).filter((book: Book) => book.bookId == id)[0];
  }

  addBook(book: Book): void {
    this.http
      .post<Book>(this.booksUrl, book, this.httpOptions)
      .subscribe((serverAnswer: Book) =>
        serverAnswer ? this.addBookToCache(serverAnswer) : {}
      );
  }

  updateBook(book: Book): void {
    this.http
      .put<Book>(this.booksUrl, book, this.httpOptions)
      .subscribe((serverAnswer: Book) =>
        serverAnswer ? this.updateBookInCache(serverAnswer) : {}
      );
  }

  deleteBook(book: Book): void {
    this.http
      .delete<boolean>(this.booksUrl + '/' + book.bookId)
      .subscribe((serverAnswer: boolean) =>
        serverAnswer ? this.deleteBookFromCache(book.bookId) : {}
      );
  }

  getBookStatus(): BehaviorSubject<Book[]> {
    return this.BooksBehaviorSubject;
  }

  updateBookProperty(
    authorId: number,
    replacementData: { [propertyName: string]: string | number }
  ): void {
    this.http
      .put<Book>(this.booksUrl + '/' + authorId, replacementData)
      .subscribe((serverAnswer) =>
        serverAnswer ? this.updateBookInCache(serverAnswer) : {}
      );
  }

  private updateBookInCache(book: Book) {
    const modifiedBookList = [...this.Books];
    modifiedBookList.splice(
      modifiedBookList.map((book) => book.bookId).indexOf(book.bookId),
      1,
      book
    );
    this.Books = [...modifiedBookList];
    this.BooksBehaviorSubject.next([...modifiedBookList]);
  }

  private addBookToCache(bookToAdd: Book) {
    this.Books = [...this.Books, bookToAdd];
    this.BooksBehaviorSubject.next(this.Books);
  }

  private deleteBookFromCache(bookId: number) {
    const modifiedBookList = [...this.Books];
    modifiedBookList.splice(
      modifiedBookList.map((book: Book) => book.bookId).indexOf(bookId),
      1
    );
    this.Books = modifiedBookList;
    this.BooksBehaviorSubject.next(modifiedBookList);
    this.userService.updateAllUserLists();
  }
}
