export interface Field {
  title: string;
  propertyName: string;
  initialValue?: string | number;
  dropDownValues?: number[];
}
