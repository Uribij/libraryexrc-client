import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../userDto';
import { Field } from '../field.interface';
import { ModalCallback } from '../modalCallback';

@Component({
  selector: 'app-form-dialogue',
  templateUrl: './form-dialogue.component.html',
  styleUrls: ['./form-dialogue.component.scss'],
})
export class FormDialogueComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<FormDialogueComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      //This modal recieves an array describing the fields you want,an identifier for the property since you'll want to change it if you are editing
      // and a callback function to return the user input
      propertyIdentification?: number;
      callBack: ModalCallback;
      fields: Field[];
    }
  ) {}

  canConfirm!: boolean;
  modalFormControl = new FormGroup({});
  ngOnInit(): void {
    this.data.fields.forEach((field) =>
      this.modalFormControl.addControl(
        field.propertyName,
        new FormControl(field.initialValue, Validators.required)
      )
    );
    this.modalFormControl.valueChanges.subscribe(() => {
      this.canConfirm = this.modalFormControl.valid;
    });
  }

  handleConfirm() {
    this.data.propertyIdentification
      ? this.data.callBack(
          this.modalFormControl.value,
          this.data.propertyIdentification
        )
      : this.data.callBack(this.modalFormControl.value);

    this.dialogRef.close();
  }
}
