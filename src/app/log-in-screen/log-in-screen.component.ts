import { Component, OnInit } from '@angular/core';
import { User } from '../userDto';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-log-in-screen',
  templateUrl: './log-in-screen.component.html',
  styleUrls: ['./log-in-screen.component.scss'],
})
export class LogInScreenComponent implements OnInit {
  constructor(private usersService: UsersService) {}
  selectedUser: User;
  users: User[] = [];
  ngOnInit(): void {
    this.getUsers();
  }
  getUsers() {
    this.usersService.getUsersBehaviorSubject().subscribe((users) => (this.users = users));
  }
  sendUser() {
    if (this.selectedUser) {
      this.usersService.logIn(this.selectedUser);
    }
  }
}
