import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementAndCreationComponent } from './management-and-creation.component';

describe('ManagementAndCreationComponent', () => {
  let component: ManagementAndCreationComponent;
  let fixture: ComponentFixture<ManagementAndCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementAndCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementAndCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
