import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Author } from '../author.dto';
import { AuthorService } from '../author.service';
import { BookService } from '../book.service';
import { Book } from '../bookDto';
import { Field } from '../field.interface';
import { FormDialogueComponent } from '../form-dialogue/form-dialogue.component';
import { ModalCallback } from '../modalCallback';
import { User } from '../userDto';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-management-and-creation',
  templateUrl: './management-and-creation.component.html',
  styleUrls: ['./management-and-creation.component.scss'],
})
export class ManagementAndCreationComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private bookService: BookService,
    private authorService: AuthorService,
    private userService: UsersService
  ) {}

  selected!: Book | Author;
  relatedData: User[] | Book[];
  managedData: Book[] | Author[];
  pageType!: 'books' | 'authors';
  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      this.pageType = data.managingType;
      if (this.pageType === 'books') {
        this.bookService.getBookStatus().subscribe((books: Book[]) => {
          this.managedData = books;
          this.selected = this.selected
            ? this.managedData
                .map((book: Book) => book.bookId)
                .indexOf((this.selected as Book).bookId) >= 0
              ? {
                  ...this.managedData[
                    this.managedData
                      .map((book: Book) => book.bookId)
                      .indexOf((this.selected as Book).bookId)
                  ],
                }
              : this.selected
            : this.selected;
        });
      } else {
        this.authorService
          .getAuthorBehaviorSubject()
          .subscribe((authors: Author[]) => {
            this.managedData = authors;
            this.selected = this.selected
              ? this.managedData
                  .map((author: Author) => author.authorId)
                  .indexOf((this.selected as Author).authorId) >= 0
                ? {
                    ...this.managedData[
                      this.managedData
                        .map((author: Author) => author.authorId)
                        .indexOf((this.selected as Author).authorId)
                    ],
                  }
                : this.selected
              : this.selected;
          });
      }
    });
  }

  selectCard(selectedData: Book | Author) {
    this.selected = selectedData;
    this.getRelatedData(
      this.pageType === 'books'
        ? (this.selected as Book).bookId
        : (this.selected as Author).authorId
    );
  }

  getRelatedData(id: number) {
    if (this.pageType === 'books') {
      this.userService
        .getUsersBehaviorSubject()
        .subscribe(
          (users: User[]) =>
            (this.relatedData = users.filter((user: User) =>
              user.bookList.includes((this.selected as Book).bookId)
            ))
        );
    } else {
      this.bookService
        .getBookStatus()
        .subscribe(
          (books: Book[]) =>
            (this.relatedData = books.filter(
              (book: Book) => book.author == (this.selected as Author).authorId
            ))
        );
    }
  }

  deleteItem(itemToDelete: Book | Author) {
    this.pageType === 'books'
      ? this.bookService.deleteBook(itemToDelete as Book)
      : this.authorService.deleteAuthor(itemToDelete as Author);
  }

  async openEditItemDialog(itemToEdit: Book | Author) {
    let propertyIdentification: number = itemToEdit['authorId'];
    const fields: Field[] = [
      {
        title: `שם ${this.pageType === 'books' ? 'הספר' : 'הסופר'}`,
        propertyName: this.pageType === 'books' ? 'bookName' : 'authorName',
        initialValue:
          this.pageType === 'books'
            ? itemToEdit['bookName']
            : itemToEdit['authorName'],
      },
    ];
    if (this.pageType === 'books') {
      propertyIdentification = (itemToEdit as Book).bookId;
      fields.push({
        title: 'סופרים',
        propertyName: 'author',
        initialValue: itemToEdit['author'],
        dropDownValues: (await this.authorService.getAuthors()).map(
          (author) => author.authorId
        ),
      });
    }
    this.openDialog(this.editItem.bind(this), fields, propertyIdentification);
  }

  async openCreateItemDialog() {
    const fields: Field[] = [
      {
        title: `שם ${this.pageType === 'books' ? 'הספר' : 'הסופר'}`,
        propertyName: this.pageType === 'books' ? 'bookName' : 'authorName',
      },
    ];
    if (this.pageType === 'books') {
      fields.push({
        title: 'סופרים',
        propertyName: 'author',
        dropDownValues: (await this.authorService.getAuthors()).map(
          (author) => author.authorId
        ),
      });
    } else {
      fields.push({
        title: 'מספר מזהה',
        propertyName: 'authorId',
      });
    }
    this.openDialog(this.addItem.bind(this), fields);
  }

  addItem(item: Book | Author) {
    if (this.pageType === 'books') {
      this.bookService.addBook(item as Book);
    } else {
      this.authorService.addAuthor(item as Author);
    }
  }

  editItem(item: { [key: string]: string | number }, identification: number) {
    if (this.pageType === 'books') {
      this.bookService.updateBookProperty(identification, { ...item });
    } else {
      this.authorService.updateAuthorProperty(identification, { ...item });
    }
  }

  openDialog(
    callback: ModalCallback,
    fields: Field[],
    propertyIdentification?: number
  ) {
    const dialogRef = this.dialog.open(FormDialogueComponent, {
      width: '40vw',
      height: 'auto',
      data: {
        callBack: callback,
        fields: fields,
        propertyIdentification,
      },
    });
  }
  
  deleteRelatedItem(relatedItem: Book | User) {
    if (this.pageType === 'books') {
      relatedItem = relatedItem as User;
      this.selected = this.selected as Book;
      relatedItem.bookList.splice(
        relatedItem.bookList.indexOf(this.selected.bookId),
        1
      );
      this.userService.updateUserData(relatedItem.userId, {
        bookList: relatedItem.bookList,
      });
    } else {
      relatedItem = relatedItem as Book;
      this.bookService.updateBookProperty(relatedItem.bookId, { author: 0 });
    }
  }
}
