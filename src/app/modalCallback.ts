export type ModalCallback = (
  userInput: { [propertyName: string]: string },
  indentification?: number
) => void;
