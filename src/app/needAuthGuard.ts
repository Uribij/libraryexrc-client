import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from './users.service';

@Injectable()
export class NeedAuthGuard implements CanActivate{
    constructor(private usersService:UsersService,private router:Router){}
    canActivate(): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if(!this.usersService.isLogged()){
            this.router.navigateByUrl(
                this.router.createUrlTree(
                  ['/login']
                )
              );
              return false;
        }else{
            return true;
        }
    }
}