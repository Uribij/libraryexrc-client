import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BookListDialougeComponent } from '../book-list-dialouge/book-list-dialouge.component';
import { FormDialogueComponent } from '../form-dialogue/form-dialogue.component';
import { User } from '../userDto';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent implements OnInit {
  constructor(private usersService: UsersService, private dialog: MatDialog) {
    this.usersService.getUsersBehaviorSubject().subscribe((users) => {
      this.users = users;
      this.selectedUser = this.selectedUser
        ? users.filter((user) => user.userId === this.selectedUser.userId)[0]
        : this.selectedUser;
    });
  }

  ngOnInit(): void {
    this.loggedInUser = this.usersService.getCurrentUser();
  }

  users!: User[];
  selectedUser!: User;
  loggedInUser: User;

  selectUser(user: User) {
    this.selectedUser = user;
  }

  changeFavoriteBook(bookId: number) {
    let replacementData = {
      favoriteBook: this.selectedUser.favoriteBook === bookId ? 0 : bookId,
    };
    this.usersService.updateUserData(
      this.selectedUser.userId,
      replacementData
    );
  }

  deleteBookFromList(bookId: number) {
    let replacementData = {};
    const bookList = [...this.selectedUser.bookList];
    if (this.selectedUser.favoriteBook === bookId) {
      replacementData = { favoriteBook: 0 };
    }
    bookList.splice(bookList.indexOf(bookId), 1);
    replacementData = { ...replacementData, bookList };
    this.usersService.updateUserData(
      this.selectedUser.userId,
      replacementData
    );
  }

  deleteUser(user: User) {
    this.usersService.deleteUser(user);
  }

  editUser(newName: string) {
    let replacementData = { userName: newName };
  }

  addBooks(bookIds: number[]) {
    if (bookIds.length) {
      let replacementData = {
        bookList: [...this.selectedUser.bookList, ...bookIds],
      };
      this.usersService.updateUserData(
        this.selectedUser.userId,
        replacementData
      );
    }
  }

  changeUsername(userInput: { userName: string }, userId: number) {
    if (
      userInput.userName !==
      this.users.filter((user) => user.userId === userId)[0].userName
    ) {
      this.usersService.updateUserData(userId, userInput);
    }
  }

  openAddBookDialog(): void {
    const dialogRef = this.dialog.open(BookListDialougeComponent, {
      width: '40vw',
      height: '90vh',
      data: {
        callBack: this.addBooks.bind(this),
        userBookList: this.selectedUser.bookList,
      },
    });
  }

  openEditUsernameDialog(user: User): void {
    const dialogRef = this.dialog.open(FormDialogueComponent, {
      width: '40vw',
      height: 'auto',
      data: {
        propertyIdentification: user.userId,
        callBack: this.changeUsername.bind(this),
        fields: [
          {
            title: 'שם משתמש',
            propertyName: 'userName',
            initialValue: user.userName,
          },
        ],
      },
    });
  }
}
