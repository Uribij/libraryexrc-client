import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { User } from './userDto';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private userssUrl = 'http://localhost:3000/user';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  constructor(private http: HttpClient, private router: Router) {}
  private currUser: User;
  private loginStatus: BehaviorSubject<User> = new BehaviorSubject<User>(
    undefined as User
  );
  private allUsers: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  //Need to only get users Once.

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userssUrl);
  }

  getUsersById(id: number): Observable<User> {
    return this.http.get<User>(this.userssUrl + `/${id}`);
  }

  addUser(user: User): void {
    this.http
      .post<User>(this.userssUrl, user, this.httpOptions)
      .subscribe((answer) =>
        answer ? this.allUsers.next([...this.allUsers.getValue(), answer]) : {}
      );
  }

  deleteUser(user: User): void {
    this.http
      .delete<boolean>(this.userssUrl + '/' + user.userId)
      .subscribe((answer) => (answer ? this.deleteUserFromCache(user) : {}));
  }

  logIn(user: User): void {
    this.updateLoggedInUser({ ...user });
    if (user) {
      this.router.navigateByUrl(`/users`);
    }
  }

  logOut(): void {
    this.updateLoggedInUser(undefined as User);
    this.router.navigateByUrl(`/login`);
  }

  isLogged(): boolean {
    return !!this.currUser;
  }

  getCurrentUser(): User {
    return this.currUser;
  }

  followLoginStatus(): BehaviorSubject<User> {
    return this.loginStatus;
  }

  getUsersBehaviorSubject(): BehaviorSubject<User[]> {
    return this.allUsers;
  }

  updateAllUsers(): void {
    this.getUsers().subscribe((users) => {
      this.allUsers.next(users);
      this.currUser
        ? users.forEach((user) =>
            user.userId === this.currUser.userId ? this.logIn(user) : {}
          )
        : {};
    });
  }

  updateAllUserLists(): void {
    this.getUsers().subscribe((users) => {
      this.allUsers.next(users);
    });
  }

  updateUserData(
    userId: number,
    replacementData: { [propertyName: string]: string | number | number[] }
  ): void {
    this.http
      .put<User>(this.userssUrl + '/' + userId, replacementData)
      .subscribe((answer) => (answer ? this.updateUserInCache(answer) : {}));
  }

  private updateUserInCache(userToUpdate: User): void {
    const userList = [...this.allUsers.getValue()];
    userList.splice(
      userList.map((user: User) => user.userId).indexOf(userToUpdate.userId),
      1,
      userToUpdate
    );
    this.allUsers.next(userList);
    userList.forEach((user: User) =>
      user.userId === this.currUser.userId
        ? this.updateLoggedInUser({ ...user })
        : {}
    );
  }

  private updateLoggedInUser(user: User): void {
    this.currUser = user;
    this.loginStatus.next(user);
  }

  private deleteUserFromCache(userToDelete: User): void {
    const userList = [...this.allUsers.getValue()];
    userList.splice(
      userList.map((user: User) => user.userId).indexOf(userToDelete.userId),
      1
    );
    this.allUsers.next(userList);
    this.currUser.userId === userToDelete.userId ? this.logOut() : {};
  }

  deleteBookFromLists(bookId: number) {
    const modifiedUsers = [...this.allUsers.getValue()].map((user: User) => ({
      ...user,
      bookList: user.bookList.splice(user.bookList.indexOf(bookId), 1),
    }));
  }
}
